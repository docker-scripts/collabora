cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

cmd_create() {
    # add DOMAIN to revproxy
    ds revproxy add
    ds revproxy ssl-cert

    # remove the container if it exists
    ds stop
    docker network disconnect $NETWORK $CONTAINER 2>/dev/null
    docker rm $CONTAINER 2>/dev/null

    # create a new container
    docker create --name=$CONTAINER --hostname=$CONTAINER \
        --restart=unless-stopped \
        --network $NETWORK --network-alias $CONTAINER \
        --cap-add MKNOD \
        --env "domain=$NC_DOMAINS" \
        "$@" $IMAGE
    ds start
}
